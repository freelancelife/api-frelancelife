const { Pool } = require('pg');

const connectionString = "postgresql://frelance_life_user:1234567890@localhost:5432/frelance_life_bd";
const DATABASE_URL = process.env.DATABASE_URL

const pool = new Pool({
  connectionString: DATABASE_URL || connectionString,
});

function query(sql, params) {
  return new Promise((resolve, reject) => {
    return pool.query(sql, params, (err, res) => {
      if(err) {
        return reject(err);
      }

      return resolve(res);
    });
  })
}

module.exports = {
  query 
}
