const pg = require('./pg');

module.exports = {
  get,
  insert
}

function create() {
  var sql = 'CREATE TABLE tasks (' 
  + 'id serial,'                    // 4 байта
  + 'title text,'
  + 'description text,'
  + "type varchar(5) DEFAULT 'item'," // item | group
  + 'parent smallint,'              // 2 байта
  + 'date_create timestamp DEFAULT CURRENT_TIMESTAMP,'     //8 байт
  + 'date_cange_last timestamp DEFAULT CURRENT_TIMESTAMP'  //8 байт
  + ');';

  return pg.query(sql, []);
}

//create()
//.then(console.log)
//.catch(console.log);

function insert(title, parent, type) {
  var sql = 'INSERT INTO tasks(title, parent, type) VALUES($1, $2, $3)';

  return pg.query(sql, [title, parent || null, type || 'item']);
}

//insert('test-1.1', 10)
//.then(console.log)
//.catch(console.log);

function deleteTasks() {
  var sql = 'DELETE FROM tasks;';

  return pg.query(sql, []);
}

//deleteTasks()
//.then(console.log)
//.catch(console.log);

function get(filter) {
  if(typeof filter === 'number') {
    let sql = 'SELECT id,title,parent,type FROM tasks where parent = $1;';
    return pg.query(sql, [filter]);
  }

  var sql = 'SELECT id,title,parent,type FROM tasks where parent is NULL;';
  return pg.query(sql, []);

}

//get(6)
//.then(res => console.log(res.rows))
//.catch(console.log);
