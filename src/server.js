const express = require('express');
const routerTasks = require('./router.tasks.js');

var app = express();

//app.use(express.json());
app.use(express.urlencoded({extended: true}));

routerTasks(app);

app.listen(5000, '0.0.0.0', function () {
  console.log('Example app listening on port 3000!');
});

//var os = require( 'os' );
//
//var networkInterfaces = os.networkInterfaces( );
//
//console.log( networkInterfaces );

