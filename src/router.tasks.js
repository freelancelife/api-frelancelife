const tasks = require('./tasks');
  
module.exports = app => {
  app.all(/tasks/, (req, res, next) => {

    if(req.headers.origin !== undefined) {
      res.set({
        'Access-Control-Allow-Origin': req.headers.origin
      });
    }

    next();

  }),

  app.get('/tasks/help', (req, res) => {
    res.send({
      'get': {
        '/tasks/:id': 'возвращает все записи c parent = id',
        '/tasks': 'Вернёт все записи с parent = null',
      },
      'post': {
        '/tasks': 'Создаёт новую запись обязательное поле titel, '
        + ' [parent] = id родителя, '
        + ' [type] = item(default) | group, '
      },
      'put': {
        '/tasks/:id': 'Обновляет Запись c указанным id'
      },
      'delete': {
        '/tasks/:id': 'Удаляет Запись c указанным id'
      }
    });
  }),

  app.get('/tasks/:id', (req, res) => {

    tasks.get(Number(req.params.id))
    .then(table => table.rows)
    .then(items => res.json({items, error: false}))
    .catch(res.send);

  }),

  app.get('/tasks', (req, res) => {
    
    tasks.get()
    .then(table => table.rows)
    .then(items => res.json({items, error: false}))
    .catch(err => res.status(500).send({error: true}));

  }),

  app.post('/tasks', (req, res) => {
    console.log('run post /tasks: ');
    let set = JSON.parse(req.body.set);

    tasks.insert(set.title, set.parent, set.type)
    .then(items => res.json({error: false}))
    .catch(err => {
      console.error(err);
      res.status(500).send({error: true})
    });
  }),

  app.put('/tasks/:id', (req, res) => {
    res.send('Hello world!');
  }),  

  app.delete('/tasks/:id', (req, res) => {
    res.send('Hello world!');
  })  
}
